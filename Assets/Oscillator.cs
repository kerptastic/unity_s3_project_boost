﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField]
    private Vector3 _movementDirection;

    [SerializeField]
    private bool _paused = false;

    //[Range(0, 1)]
    //[SerializeField]
    private float _movementFactor;

    [SerializeField]
    private float _movementCycleInSeconds;

    [SerializeField]
    private float _oscillatorAngleInDegrees = 0.0f;
    private Vector3 _initialPosition;


    // Start is called before the first frame update
    void Start()
    {
        _movementDirection = new Vector3(5.0f, 0.0f, 0.0f);
        _movementFactor = 0.0f;
        _movementCycleInSeconds = 2.0f;

        _initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        UpdateMovementFactor();

        var offset = _movementDirection * _movementFactor;
        transform.position = offset + _initialPosition;
    }

    private void UpdateMovementFactor()
    {
        if (!_paused && _movementCycleInSeconds > Mathf.Epsilon)
        {
            _oscillatorAngleInDegrees += (360.0f / _movementCycleInSeconds) * Time.deltaTime;

            // clamp to a max of 360.0f to avoid overflow
            if (_oscillatorAngleInDegrees > 360.0f)
            {
                _oscillatorAngleInDegrees -= 360.0f;
            }
            else if (_oscillatorAngleInDegrees < Mathf.Epsilon)
            {
                _oscillatorAngleInDegrees += 360.0f;
            }

            // this will be between 0 and 1
            var radians = _oscillatorAngleInDegrees * Mathf.Deg2Rad;
            var sin = Mathf.Sin(radians);

            _movementFactor = 0.5f * (1.0f + sin);
        }
    }
}
