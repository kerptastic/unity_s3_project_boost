﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    private enum Direction
    {
        Left, Right
    }

    private enum State
    {
        Alive, Dying, Landed, Winning
    }

    [SerializeField]
    private static float LevelLoadDelay = 2.0f;

    [SerializeField]
    private static bool CollisionsEnabled = true;

    private Rigidbody _rigidBody;
    private AudioSource _audioSource;

    [SerializeField]
    private AudioClip _acEngine;

    [SerializeField]
    private AudioClip _acCrashed;

    [SerializeField]
    private AudioClip _acLanded;

    [SerializeField]
    private AudioClip _acNoFuel;

    [SerializeField]
    private ParticleSystem _psCrashed;

    [SerializeField]
    private ParticleSystem _psLanded;

    [SerializeField]
    private ParticleSystem _psEngine;

    [SerializeField]
    private ParticleSystem _psNoFuel;

    [SerializeField]
    private float _rotateDegreesPerSecond;

    [SerializeField]
    private float _thrustForcePerSecond;

    [SerializeField]
    private float _fuelPerSecond;

    [SerializeField]
    private float _fuel;
    protected float Fuel { get { return _fuel; } set { _fuel = value; } }

    [SerializeField]
    private State _currentState;

    [SerializeField]
    private int _currentLevelIndex;

    private Vector3 _initialPosition;
    private Quaternion _initialRotation;


    // Start is called before the first frame update
    void Start()
    {
        _currentLevelIndex = SceneManager.GetActiveScene().buildIndex;

        _rigidBody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();

        _initialPosition = transform.position;
        _initialRotation = transform.rotation;

        _rotateDegreesPerSecond = 180.0f;
        _thrustForcePerSecond = 300.0f;
        _fuelPerSecond = 75.0f;

        _fuel = 1000;

        _currentState = State.Alive;
    }

    // Update is called once per frame
    void Update()
    {
        HandleResetInput();

        if (Debug.isDebugBuild)
        {
            HandleDebugInput();
        }

        if (_currentState == State.Alive)
        {
            HandleThrustInput();
            HandleRotateInput();
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        if (_currentState != State.Alive || !CollisionsEnabled)
        {
            return;
        }

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;

            case "Fuel":
                break;

            case "Finish":
                Landed();
                Invoke("LoadNextLevel", LevelLoadDelay);
                break;

            default:
                Crashed();
                Invoke("ReloadLevel", LevelLoadDelay);
                break;
        }
    }

    private void HandleResetInput()
    {
        if (Input.GetKey(KeyCode.R))
        {
            Reset();
        }
    }

    private void HandleDebugInput()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Invoke("LoadNextLevel", 0.0f);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            CollisionsEnabled = !CollisionsEnabled;
        }
    }

    private void HandleRotateInput()
    {
        // left only
        if (Input.GetKey(KeyCode.A))
        {
            Rotate(Direction.Left);
        }
        // right only
        else if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
        {
            Rotate(Direction.Right);
        }
        // both
        else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.A))
        {
            // do nothing right now
        }
    }

    private void HandleThrustInput()
    {
        if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space))
        {
            FireEngine();
        }
        else
        {
            KillEngine();
        }
    }


    private void Rotate(Direction dir)
    {
        var dirValue = dir == Direction.Left ? 1.0f : -1.0f;
        var rotationAmount = Vector3.forward * _rotateDegreesPerSecond * Time.deltaTime * dirValue;

        _rigidBody.freezeRotation = true;
        transform.Rotate(rotationAmount);
        _rigidBody.freezeRotation = false;
    }


    private void FireEngine()
    {
        if (_fuel > 0.0f)
        {
            var thrustAmount = _thrustForcePerSecond * Time.deltaTime;
            _rigidBody.AddRelativeForce(Vector3.up * thrustAmount);

            var _fuelUsed = (_fuelPerSecond * Time.deltaTime);
            _fuel = Mathf.Max(0.0f, _fuel - _fuelUsed);

            // particles
            if (!_psEngine.isPlaying)
            {
                _psEngine.Play();
            }

            // audio
            if (!_audioSource.isPlaying)
            {
                _audioSource.PlayOneShot(_acEngine);
            }
        }
        else
        {
            _psEngine.Stop();

            // audio
            if (!_audioSource.isPlaying)
            {
                _audioSource.PlayOneShot(_acNoFuel);
            }
        }
    }

    private void KillEngine()
    {
        _audioSource.Stop();
        _psEngine.Stop();
    }

    private void Crashed()
    {
        _currentState = State.Dying;

        KillEngine();

        _psCrashed.Play();

        _audioSource.Stop();
        _audioSource.PlayOneShot(_acCrashed);
    }

    private void Landed()
    {
        _currentState = State.Landed;

        KillEngine();

        _psLanded.Play();

        _audioSource.Stop();
        _audioSource.PlayOneShot(_acLanded);
    }

    private void Reset()
    {
        _currentState = State.Alive;

        transform.position = _initialPosition;
        transform.rotation = _initialRotation;

        _rigidBody.velocity = Vector3.zero;
        _fuel = 1000;
    }

    private void ReloadLevel()
    {
        _currentState = State.Alive;

        SceneManager.LoadScene(_currentLevelIndex);
    }

    private void LoadNextLevel()
    {
        _currentState = State.Alive;

        _currentLevelIndex++;

        if (_currentLevelIndex > SceneManager.sceneCount)
        {
            _currentLevelIndex = 0;
        }

        SceneManager.LoadScene(_currentLevelIndex);
    }
}
